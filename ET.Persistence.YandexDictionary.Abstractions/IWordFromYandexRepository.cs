﻿using ET.Domain.DomainEntities.Word;
using System.Threading.Tasks;

namespace ET.Persistence.YandexDictionary.Abstractions
{
    public interface IWordFromYandexRepository
    {
        Task<WordFromYandex> GetWordAsync(WordFromDB word);
    }
}