﻿using ET.Domain.DomainEntities.Rating;
using ET.Domain.DomainEntities.Word;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ET.Persistence.Abstractions
{
    public interface IWordRepository
    {
        Task<WordDomain> GetWordAsync(int wordId);

        Task<List<WordIdRating>> GetWordIdsRatings();

        Task<byte[]> GetPrononsiationAsync(int wordId);

        Task UpdateRatingAsync(RatingDomain rating);
    }
}