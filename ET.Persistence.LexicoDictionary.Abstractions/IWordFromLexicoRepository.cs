﻿using ET.Domain.DomainEntities.Word;
using System.Threading.Tasks;

namespace ET.Persistence.LexicoDictionary.Abstractions
{
    public interface IWordFromLexicoRepository
    {
        Task<WordFromLexico> GetWordAsync(WordFromDB word);
    }
}