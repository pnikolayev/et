﻿using ET.Business.Abstractions;
using ET.Business.Extensions;
using ET.Domain.DomainEntities.Rating;
using ET.Domain.DomainEntities.Word;
using ET.Persistence.Abstractions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ET.Business.Services
{
    public class WordService : IWordService
    {
        private readonly IWordRepository _wordRepository;

        public WordService(IWordRepository wordRepository)
        {
            _wordRepository = wordRepository;
        }

        public async Task<WordDomain> GetWordAsync(int wordId)
        {
            return await _wordRepository.GetWordAsync(wordId);
        }

        public async Task<WordDomain> GetRandomWordAsync()
        {
            Random rng = new Random((int)DateTime.Now.TimeOfDay.TotalSeconds);
            var windowWidth = 500;

            var wordIdsRatings = await _wordRepository.GetWordIdsRatings();

            var curve = wordIdsRatings
                .Shuffle()
                .OrderByDescending(x => (DateTime.Now - x.LastRepeat).TotalHours / x.Rating)
                .Select(x => x.WordId)
                .ToList();

            var wordId = curve.ElementAt(rng.Next(windowWidth));

            return await GetWordAsync(wordId);
        }

        public async Task<byte[]> GetPrononsiationAsync(int wordId)
        {
            return await _wordRepository.GetPrononsiationAsync(wordId);
        }

        public async Task UpdateRatingAsync(RatingDomain rating)
        {
            await _wordRepository.UpdateRatingAsync(rating);
        }
    }
}