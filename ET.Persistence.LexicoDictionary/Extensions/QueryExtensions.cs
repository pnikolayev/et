﻿using ET.Domain.DomainEntities.Designation;
using System.Collections.Generic;
using System.Linq;

namespace ET.Persistence.LexicoDictionary.Extensions
{
    internal static class QueryExtensions
    {
        internal static List<DesignationFromLexico> FilterList(this List<DesignationFromLexico> posListLexico, List<string> posListFile)
        {
            var filtredPosListYandex = new List<DesignationFromLexico>();

            foreach (var pos in posListFile)
            {
                var groupedResult = posListLexico.Where(x => x.PartOfSpeech == pos).SingleOrDefault();

                if (groupedResult == null)
                {
                    filtredPosListYandex.Add(new DesignationFromLexico() { PartOfSpeech = pos });// mapper???
                }
                else
                {
                    filtredPosListYandex.Add(groupedResult);
                }
            }

            return filtredPosListYandex;
        }
    }
}