﻿using ET.Domain.DomainEntities.Designation;
using HtmlAgilityPack;
using System.Collections.Generic;

namespace ET.Persistence.LexicoDictionary.Extensions
{
    internal static class HtmlAgilityPackExtensions
    {
        internal static List<DesignationFromLexico> GetLexicoDefinitions(this HtmlNodeCollection sectionModes)
        {
            var lexicoDefinitions = new List<DesignationFromLexico>();

            foreach (var sectionNode in sectionModes)
            {
                string partsOfSpeach = "";
                string firstDefinition = "";

                var partsOfSpeachNode = sectionNode.SelectSingleNode(".//span[contains(@class, 'pos')]");
                if (partsOfSpeachNode != null)
                {
                    partsOfSpeach = partsOfSpeachNode.InnerText;
                }

                var firstDefinitionNode = sectionNode.SelectSingleNode(".//span[contains(@class, 'ind')]");
                if (firstDefinitionNode != null)
                {
                    firstDefinition = firstDefinitionNode.InnerText;
                }

                lexicoDefinitions.Add(new DesignationFromLexico() { Definition = firstDefinition, PartOfSpeech = partsOfSpeach });
            }

            return lexicoDefinitions;
        }

        internal static HtmlNodeCollection SelectNodesByClassesNames(this HtmlNode initialNode, string[] classesNames)
        {
            HtmlNodeCollection newNodes = new HtmlNodeCollection((HtmlNode)null);

            foreach (var node in initialNode.ChildNodes)
            {
                for (int i = 0; i < classesNames.Length; i++)
                {
                    if (node.OuterHtml.Contains("class=\"" + classesNames[i] + "\""))
                    {
                        newNodes.Add(node);
                    }
                }
            }

            return newNodes;
        }

        internal static (HtmlNodeCollection sectionNodes, HtmlNode audioNode) FilterNodesByMainDefinition(this HtmlNodeCollection nodes, int index)
        {
            List<List<HtmlNode>> listList = new List<List<HtmlNode>>();
            List<HtmlNode> list = new List<HtmlNode>();

            for (int i = 1; i < nodes.Count; i++)
            {
                if (nodes[i].Name == "section")
                {
                    list.Add(nodes[i]);
                }
                else
                {
                    listList.Add(list);
                    list = new List<HtmlNode>();
                }
            }
            listList.Add(list);

            List<HtmlNode> filtredList = listList[index - 1];

            HtmlNode audio = filtredList[filtredList.Count - 1];
            filtredList.Remove(audio);

            HtmlNodeCollection newNodes = new HtmlNodeCollection((HtmlNode)null);

            foreach (var node in filtredList)
            {
                newNodes.Add(node);
            }

            return (sectionNodes: newNodes, audioNode: audio);
        }
    }
}