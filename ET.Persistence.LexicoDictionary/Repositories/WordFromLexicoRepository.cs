﻿using ET.Domain.DomainEntities.Word;
using ET.Domain.Extensions;
using ET.Persistence.LexicoDictionary.Abstractions;
using ET.Persistence.LexicoDictionary.Extensions;
using HtmlAgilityPack;
using System.Data;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ET.Persistence.LexicoDictionary.Repositories
{
    public class WordFromLexicoRepository : IWordFromLexicoRepository
    {
        private readonly string _lexicoUrl;

        public WordFromLexicoRepository(string lexicoUrl)
        {
            _lexicoUrl = lexicoUrl;
        }

        public async Task<WordFromLexico> GetWordAsync(WordFromDB word)
        {
            var value = word.Value;

            var wordUrl = _lexicoUrl + value.Split(new char[] { ' ', ',' }).First().RemoveIndex();

            using var client = new WebClient();
            string htmlPage = client.DownloadString(wordUrl);

            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlPage);

            var entryWrapperNode = htmlDoc.DocumentNode.SelectSingleNode("//div[contains(@class, 'entryWrapper')]");
            var mainDefinitionNodes = entryWrapperNode.SelectNodes(".//section[contains(@class, 'gramb')]");
            string[] classesNames = { "entryHead primary_homograph", "gramb", "pronSection etym" };

            var nodeCollection = entryWrapperNode
                .SelectNodesByClassesNames(classesNames)
                .FilterNodesByMainDefinition(value.GetIndex());

            var lexicoDefinitions = nodeCollection.sectionNodes.GetLexicoDefinitions();
            var posListFile = word.Designations.Select(x => x.PartOfSpeech).ToList();

            var audio = nodeCollection.audioNode.SelectSingleNode("//audio");
            var audioUrl = audio.Attributes.Where(x => x.Value.Contains("mp3")).FirstOrDefault().Value;

            byte[] content = await client.DownloadDataTaskAsync(audioUrl);

            return new WordFromLexico()
            {
                Value = value,
                Designations = lexicoDefinitions.FilterList(posListFile),
                Prononsiation = content
            };
        }
    }
}