﻿using ET.Domain.DomainEntities.Designation;
using ET.Persistence.YandexDictionary.Entities;
using System.Collections.Generic;
using System.Linq;

namespace ET.Persistence.YandexDictionary.Extensions
{
    internal static class QueryExtensions
    {
        internal static List<DesignationFromYandex> FilterList(this List<DesignationFromYandex> posListYandex, List<string> posListFile)
        {
            var filtredPosListYandex = new List<DesignationFromYandex>();

            foreach (var pos in posListFile)
            {
                var groupedResult = posListYandex.Where(x => x.PartOfSpeech == pos).SingleOrDefault();

                if (groupedResult == null)
                {
                    filtredPosListYandex.Add(new DesignationFromYandex() { PartOfSpeech = pos });// mapper???
                }
                else
                {
                    filtredPosListYandex.Add(groupedResult);
                }
            }

            return filtredPosListYandex;
        }

        internal static List<DesignationFromYandex> GroupByPOS(this List<YandexDictionaryAPIEntity> items)
        {
            return items.GroupBy(tr => tr.PartOfSpeech)
                .Select(g => new DesignationFromYandex
                {
                    PartOfSpeech = g.Key,
                    Value = g.Select(t => t.Translation).ToList().Aggregate((current, next) => current + ", " + next)
                })
                .ToList();
        }
    }
}