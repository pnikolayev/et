﻿using ET.Persistence.YandexDictionary.Entities;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;

namespace ET.Persistence.YandexDictionary.Extensions
{
    internal static class NewtonsoftJsonExtensions
    {
        internal static List<YandexDictionaryAPIEntity> GetYandexTranslation(this JObject jObject)
        {
            IEnumerable<JToken> results = jObject["def"].Children().ToList().Children();
            List<JToken> translations = new List<JToken>();

            foreach (JToken result in results)
            {
                JProperty jProperty = result.ToObject<JProperty>();

                string propertyName = jProperty.Name;
                List<JToken> propertyValues = jProperty.Value.ToList();

                if (propertyName == "syn")
                {
                    translations = translations.Concat(propertyValues).ToList();
                }
                if (propertyName == "tr")
                {
                    translations = translations.Concat(propertyValues).ToList();
                }
            }

            List<YandexDictionaryAPIEntity> yandexList = new List<YandexDictionaryAPIEntity>();
            foreach (JToken translation in translations)
            {
                yandexList.Add(translation.ToObject<YandexDictionaryAPIEntity>());
            }

            return yandexList;
        }
    }
}