﻿using ET.Domain.DomainEntities.Word;
using ET.Domain.Extensions;
using ET.Persistence.YandexDictionary.Abstractions;
using ET.Persistence.YandexDictionary.Extensions;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Linq;
using System.Threading.Tasks;

namespace ET.Persistence.YandexDictionary.Repositories
{
    public class WordFromYandexRepository : IWordFromYandexRepository
    {
        private readonly string _yandexDictionaryAPIUrl;

        public WordFromYandexRepository()
        {
        }

        public WordFromYandexRepository(string yandexDictionaryAPIUrl)
        {
            _yandexDictionaryAPIUrl = yandexDictionaryAPIUrl;
        }

        public async Task<WordFromYandex> GetWordAsync(WordFromDB word)
        {
            var value = word.Value;
            var fixedValue = value
                .Split(new char[] { ' ', ',' })
                .First()
                .RemoveIndex();

            //var wordUrl = $"{_options.Url}?key={_options.Key}&lang={_options.Lang}&text={fixedValue}";
            var wordUrl = $"{_yandexDictionaryAPIUrl}&text={fixedValue}";

            var client = new RestClient(wordUrl);
            var request = new RestRequest(Method.GET);
            IRestResponse response = await client.ExecuteAsync(request);

            if (!response.IsSuccessful)
                return null;

            //var results = JsonConvert.DeserializeObject<JToken>(response.Content).YandexTranslation();
            var results = JObject.Parse(response.Content).GetYandexTranslation();
            var designationsListYandex = results.GroupByPOS();
            var posListFile = word.Designations.Select(x => x.PartOfSpeech).ToList();

            return new WordFromYandex()
            {
                Value = value,
                Designations = designationsListYandex.FilterList(posListFile)
            };
        }
    }
}