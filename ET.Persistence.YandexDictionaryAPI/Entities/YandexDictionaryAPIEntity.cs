﻿using Newtonsoft.Json;

namespace ET.Persistence.YandexDictionary.Entities
{
    internal class YandexDictionaryAPIEntity
    {
        [JsonProperty("text")]
        internal string Translation { get; set; }

        [JsonProperty("pos")]
        internal string PartOfSpeech { get; set; }
    }
}