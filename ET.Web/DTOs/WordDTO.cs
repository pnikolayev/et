﻿using System.Collections.Generic;

namespace ET.Web.DTOs
{
    public class WordDTO
    {
        public int WordId { get; set; }
        public string Value { get; set; }
        public int Rating { get; set; }
        public List<DesignationDTO> Designations { get; set; }
    }
}