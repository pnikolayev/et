﻿namespace ET.Web.DTOs
{
    public class DesignationDTO
    {
        public string Value { get; set; }
        public string Level { get; set; }
        public string PartOfSpeech { get; set; }
        public string Definition { get; set; }
    }
}