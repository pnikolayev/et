﻿namespace ET.Web.DTOs
{
    public class RatingDTO
    {
        public int Rating { get; set; }
    }
}