﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace ET.Web.Extensions
{
    internal static class AutoMapperExtensions
    {
        internal static IServiceCollection AddAutoMapperService(this IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(assembly => assembly.GetTypes())
                       .Where(type => type.IsSubclassOf(typeof(Profile)))
                       .Select(type => type.Assembly);

            return services.AddAutoMapper(assemblies);
        }
    }
}