﻿using ET.Persistence;
using Microsoft.EntityFrameworkCore;

//using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ET.Web.Extensions
{
    internal static class IHostExtensions
    {
        internal static IHost SeedData(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                using (var appContext = scope.ServiceProvider.GetRequiredService<DataContext>())
                {
                    //appContext.Database.Migrate();
                    appContext.Database.EnsureCreated();
                }
            }

            return host;
        }
    }
}