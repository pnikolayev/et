﻿using ET.Business.Abstractions;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ET.Web.Controllers
{
    [Route("api/mp3")]
    [ApiController]
    public class AudioController : ControllerBase
    {
        private readonly IWordService _wordService;

        public AudioController(IWordService wordService)
        {
            _wordService = wordService;
        }

        [HttpGet("{wordId}")]
        public async Task<ActionResult> GetRandomWord(int wordId)
        {
            var audio = await _wordService.GetPrononsiationAsync(wordId);

            if (audio == null)
                return BadRequest();

            return File(audio, "audio/mpeg");
        }
    }
}