﻿using AutoMapper;
using ET.Business.Abstractions;
using ET.Domain.DomainEntities.Word;
using ET.Web.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ET.Web.Controllers
{
    [Route("api/random-word")]
    [ApiController]
    public class WordController : ControllerBase
    {
        private readonly IWordService _wordService;
        private readonly IMapper _mapper;

        public WordController(IWordService wordService, IMapper mapper)
        {
            _wordService = wordService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<WordDTO>> GetRandomWord()
        {
            var word = await _wordService.GetRandomWordAsync();

            return Ok(_mapper.Map<WordDomain, WordDTO>(word));
        }
    }
}