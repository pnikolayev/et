﻿using AutoMapper;
using ET.Domain.DomainEntities.Designation;
using ET.Domain.DomainEntities.Rating;
using ET.Domain.DomainEntities.Word;
using ET.Web.DTOs;

namespace ET.Web.Controllers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<WordDomain, WordDTO>();

            CreateMap<DesignationDomain, DesignationDTO>();

            CreateMap<RatingDTO, RatingDomain>()
                .ForMember(dest => dest.WordId, opt => opt.Ignore());
        }
    }
}
