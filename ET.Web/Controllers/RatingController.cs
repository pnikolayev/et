﻿using AutoMapper;
using ET.Business.Abstractions;
using ET.Domain.DomainEntities.Rating;
using ET.Web.DTOs;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ET.Web.Controllers
{
    [Route("api/rating")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly IWordService _wordService;
        private readonly IMapper _mapper;

        public RatingController(IWordService wordService, IMapper mapper)
        {
            _wordService = wordService;
            _mapper = mapper;
        }

        [HttpPost("{wordId}")]
        public async Task<ActionResult> UpdateRating([FromBody] RatingDTO ratingDTO, int wordId)
        {
            var rating = _mapper.Map<RatingDTO, RatingDomain>(ratingDTO);
            rating.WordId = wordId;

            await _wordService.UpdateRatingAsync(rating);
            return Ok();
        }
    }
}