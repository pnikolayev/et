﻿namespace ET.Domain.DomainEntities.Designation
{
    public class DesignationDomain
    {
        public int DesignationId { get; set; }
        public string Value { get; set; }
        public string Level { get; set; }
        public string PartOfSpeech { get; set; }
        public string Definition { get; set; }
    }
}