﻿namespace ET.Domain.DomainEntities.Rating
{
    public class RatingDomain
    {
        public int WordId { get; set; }
        public int Rating { get; set; }
    }
}