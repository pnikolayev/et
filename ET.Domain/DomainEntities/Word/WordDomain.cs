﻿using ET.Domain.DomainEntities.Designation;
using System;
using System.Collections.Generic;

namespace ET.Domain.DomainEntities.Word
{
    public class WordDomain
    {
        public int WordId { get; set; }
        public string Value { get; set; }
        public int Rating { get; set; }
        public DateTime LastRepeat { get; set; }
        public byte[] Prononsiation { get; set; }
        public bool IsFilled { get; set; }

        public List<DesignationDomain> Designations { get; set; }
    }
}