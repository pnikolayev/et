﻿using ET.Domain.DomainEntities.Designation;
using System.Collections.Generic;

namespace ET.Domain.DomainEntities.Word
{
    public class WordSeed
    {
        public int WordId { get; set; }
        public string Value { get; set; }
        public byte[] Prononsiation { get; set; }
        public List<DesignationDomain> Designations { get; set; }
    }
}