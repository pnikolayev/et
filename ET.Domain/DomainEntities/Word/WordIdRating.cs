﻿using System;

namespace ET.Domain.DomainEntities.Word
{
    public class WordIdRating
    {
        public int WordId { get; set; }
        public int Rating { get; set; }
        public DateTime LastRepeat { get; set; }
    }
}