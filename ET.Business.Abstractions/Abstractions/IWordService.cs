﻿using ET.Domain.DomainEntities.Rating;
using ET.Domain.DomainEntities.Word;
using System.Threading.Tasks;

namespace ET.Business.Abstractions
{
    public interface IWordService
    {
        Task<WordDomain> GetWordAsync(int wordId);

        Task<WordDomain> GetRandomWordAsync();

        Task<byte[]> GetPrononsiationAsync(int wordId);

        Task UpdateRatingAsync(RatingDomain rating);
    }
}