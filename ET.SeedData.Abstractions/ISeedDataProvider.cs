﻿using ET.Domain.DomainEntities.Word;
using System.Collections.Generic;

namespace ET.SeedData.Abstractions
{
    public interface ISeedDataProvider
    {
        List<WordSeed> GetSeedData();
    }
}