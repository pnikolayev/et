﻿using ET.Domain.DomainEntities.Word;
using ET.SeedData.Abstractions;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ET.SeedData
{
    public class SeedDataProvider : ISeedDataProvider
    {
        public List<WordSeed> GetSeedData()
        {
            var seedDataJson = SeedData.SeedDataJson;
            return JsonConvert.DeserializeObject<List<WordSeed>>(seedDataJson);
        }
    }
}