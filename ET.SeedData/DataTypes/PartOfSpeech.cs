﻿using System.Collections.Generic;

namespace ET.SeedData.DataTypes
{
    internal class PartOfSpeech
    {
        internal static Dictionary<string, string> Dictionary;

        static PartOfSpeech()
        {
            Dictionary = new Dictionary<string, string>
            {
                { "indefinite article",  "determiner"},
                { "definite article",  "definite article"},
                { "modal v.", "modal verb" },
                { "auxiliary v.", "auxiliary verb" },
                { "exclam.", "exclamation" },
                { "number", "number" },
                { "adj.", "adjective" },
                { "adv.", "adverb" },
                { "prep.", "preposition" },
                { "conj.", "conjunction " },
                { "pron.", "pronoun" },
                { "det.", "determiner" },
                { "v.", "verb" },
                { "n.", "noun" },
            };
        }
    }
}