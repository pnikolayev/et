﻿using ET.SeedData.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ET.SeedData
{
    internal static class SeedDataProviderExtensions
    {
        internal static List<string> SuperSplit(this string info)
        {
            var dictionary = PartOfSpeech.Dictionary;
            var list = new List<string>();

            foreach (var entry in dictionary)
            {
                if (info.Contains(entry.Key))
                {
                    info = info.Replace(entry.Key, "");
                    list.Add(entry.Key);
                }
            }

            foreach (Level level in Enum.GetValues(typeof(Level)))
            {
                var levelString = level.ToString();
                if (info.Contains(levelString))
                {
                    list.Add(levelString);
                }
            }

            return list;
        }

        internal static (string token, string remain) StrTok(this string word)
        {
            var dictionary = PartOfSpeech.Dictionary;
            var indeces = new List<int>();

            foreach (var entry in dictionary)
            {
                if (word.Contains(entry.Key))
                {
                    var index = word.IndexOf(entry.Key);

                    if (index > 0)
                    {
                        indeces.Add(index);
                    }
                }
            }

            int length = indeces.Min();

            return (token: word.Substring(0, length).Trim(),
                remain: word.Substring(length, word.Length - length).Trim());
        }
    }
}