﻿using AutoMapper;
using ET.Domain.DomainEntities.Rating;
using ET.Domain.DomainEntities.Word;
using ET.Persistence.Abstractions;
using ET.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ET.Persistence.Repositories
{
    public class WordRepository : IWordRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public WordRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<WordDomain> GetWordAsync(int wordId)
        {
            var wordDB = await _context.Words
                .Include(w => w.Designations)
                .Where(x => x.WordId == wordId)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            return _mapper.Map<Word, WordDomain>(wordDB);
        }

        public async Task<List<WordIdRating>> GetWordIdsRatings()
        {
            return await _context.Words
                .Select(item => new WordIdRating
                {
                    WordId = item.WordId,
                    Rating = item.Rating,
                    LastRepeat = item.LastRepeat
                })
                .ToListAsync();
        }

        public async Task UpdateRatingAsync(RatingDomain rating)
        {
            var wordDB = await _context.Words
                .Where(x => x.WordId == rating.WordId)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (wordDB != null)
            {
                wordDB.Rating = rating.Rating;
                _context.Words.Update(wordDB);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<byte[]> GetPrononsiationAsync(int wordId)
        {
            return await _context.Words
                .Where(x => x.WordId == wordId)
                .Select(x => x.Prononsiation)
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }
    }
}