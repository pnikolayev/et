﻿using AutoMapper;
using ET.Domain.DomainEntities.Designation;
using ET.Domain.DomainEntities.Word;
using ET.Persistence.Entities;

namespace ET.Persistence.Repositories
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Word, WordDomain>();

            CreateMap<Designation, DesignationDomain>();

            CreateMap<WordDomain, Word>();

            CreateMap<DesignationDomain, Designation>();
        }
    }
}