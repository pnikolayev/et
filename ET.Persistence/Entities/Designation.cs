﻿namespace ET.Persistence.Entities
{
    internal class Designation
    {
        public int DesignationId { get; set; }
        public int WordId { get; set; }
        public string Level { get; set; }
        public string PartOfSpeech { get; set; }
        public string Value { get; set; }
        public string Definition { get; set; }

        public Word Word { get; set; }
    }
}