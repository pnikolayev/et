﻿using System;
using System.Collections.Generic;

namespace ET.Persistence.Entities
{
    internal class Word
    {
        public Word()
        {
            Designations = new HashSet<Designation>();
        }

        public int WordId { get; set; }
        public string Value { get; set; }
        public int Rating { get; set; }
        public DateTime LastRepeat { get; set; }
        public byte[] Prononsiation { get; set; }

        public ICollection<Designation> Designations { get; set; }
    }
}