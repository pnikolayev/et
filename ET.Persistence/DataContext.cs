﻿using ET.Persistence.Configurations;
using ET.Persistence.Entities;
using ET.Persistence.Extensions;
using ET.SeedData.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace ET.Persistence
{
    public class DataContext : DbContext
    {
        private readonly ISeedDataProvider _seedDataProvider;

        public DataContext(DbContextOptions options, ISeedDataProvider seedDataProvider)
            : base(options)
        {
            _seedDataProvider = seedDataProvider;
        }

        internal DbSet<Word> Words { get; set; }
        internal DbSet<Designation> Designations { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new WordConfiguration());
            modelBuilder.ApplyConfiguration(new DesignationConfiguration());

            modelBuilder.SeedData(_seedDataProvider);
        }
    }
}