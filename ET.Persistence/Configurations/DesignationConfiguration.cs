﻿using ET.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ET.Persistence.Configurations
{
    internal class DesignationConfiguration : IEntityTypeConfiguration<Designation>
    {
        public void Configure(EntityTypeBuilder<Designation> builder)
        {
            builder
                .Property(d => d.Level)
                .HasMaxLength(2)
                .IsRequired();

            builder
                .Property(d => d.PartOfSpeech)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(d => d.Value)
                .HasMaxLength(500);

            builder
                .Property(d => d.Definition)
                .HasMaxLength(500);
        }
    }
}