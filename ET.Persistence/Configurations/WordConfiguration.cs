﻿using ET.Persistence.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ET.Persistence.Configurations
{
    internal class WordConfiguration : IEntityTypeConfiguration<Word>
    {
        public void Configure(EntityTypeBuilder<Word> builder)
        {
            builder
                .HasMany(w => w.Designations)
                .WithOne(d => d.Word)
                .HasForeignKey(d => d.WordId)
                .OnDelete(DeleteBehavior.Cascade)
                .IsRequired();

            builder
                .Property(w => w.Value)
                .HasMaxLength(50)
                .IsRequired();
        }
    }
}