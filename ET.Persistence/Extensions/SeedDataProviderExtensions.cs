﻿using ET.Domain.DomainEntities.Word;
using ET.Persistence.Entities;
using System;
using System.Collections.Generic;

namespace ET.Persistence.Extensions
{
    internal static class SeedDataProviderExtensions
    {
        public static (Word[] words, Designation[] designations) MapSeedData(this List<WordSeed> seedData)
        {
            var wordsList = new List<Word>();
            var designationsList = new List<Designation>();

            var dateTime = DateTime.Now;

            foreach (var item in seedData)
            {
                wordsList.Add(new Word()
                {
                    WordId = item.WordId,
                    Value = item.Value,
                    Rating = 1,
                    LastRepeat = dateTime,
                    Prononsiation = item.Prononsiation
                });

                foreach (var designation in item.Designations)
                {
                    designationsList.Add(new Designation()
                    {
                        WordId = item.WordId,
                        DesignationId = designation.DesignationId,
                        Level = designation.Level,
                        PartOfSpeech = designation.PartOfSpeech,
                        Definition = designation.Definition,
                        Value = designation.Value
                    });
                }
            }

            return (words: wordsList.ToArray(), designations: designationsList.ToArray());
        }
    }
}