﻿using ET.Persistence.Entities;
using ET.SeedData.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace ET.Persistence.Extensions
{
    internal static class ModelBuilderExtensions
    {
        public static void SeedData(this ModelBuilder modelBuilder, ISeedDataProvider seedDataProvider)
        {
            if (seedDataProvider != null)
            {
                var result = seedDataProvider
                    .GetSeedData()
                    .MapSeedData();

                modelBuilder.Entity<Word>().HasData(result.words);
                modelBuilder.Entity<Designation>().HasData(result.designations);
            }
        }
    }
}